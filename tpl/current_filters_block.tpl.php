<?php
/**
 * @file
 * current_filters_block.tpl
 */
?>
<div class="current-filters">
  <?php
    foreach ($filters as $filter) {
      $path = 'href="' . $filter->path . '"';
      print l(
        $filter->text_prefix . $filter->name . $filter->text_sufix,
        $filter->path,
        array(
          'query'       => $filter->query,
          'html'       => TRUE,
          'attributes' => $filter->attributes,
        )
      );
    }
  ?>
</div>
