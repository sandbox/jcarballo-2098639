<?php
/**
 * @file
 * current_filters.admin.inc
 */


/**
 * Build the admin settings form.
 */
function current_filters_admin_settings() {
  $form['current_filters']['current_filters_text_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Text Sufix'),
    '#size' => 60,
    '#default_value' => variable_get('current_filters_text_prefix'),
    '#description' => t('Accept HTML'),
  );
  $form['current_filters']['current_filters_text_sufix'] = array(
    '#type' => 'textfield',
    '#title' => t('Text Prefix'),
    '#size' => 60,
    '#default_value' => variable_get('current_filters_text_sufix'),
    '#description' => t('Accept HTML'),
  );
  $form['current_filters']['current_filters_link_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Link Class'),
    '#size' => 60,
    '#default_value' => variable_get('current_filters_link_class'),
    '#description' => t('Classes separated by space '),
  );
  return system_settings_form($form);
}
